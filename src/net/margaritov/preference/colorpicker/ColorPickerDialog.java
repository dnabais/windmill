/*
 * Copyright (C) 2010 Daniel Nilsson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.margaritov.preference.colorpicker;

import noxx.evil.windmillDevSupp.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class ColorPickerDialog 
	extends 
		Dialog 
	implements
		ColorPickerView.OnColorChangedListener,
		View.OnClickListener {

	private ColorPickerView mColorPicker;

	private ColorPickerPanelView mOldColor;
	private ColorPickerPanelView mNewColor;
	private ColorPickerPanelView mColor1;
	private ColorPickerPanelView mColor2;
	private ColorPickerPanelView mColor3;
	private ColorPickerPanelView mColor4;
	private ColorPickerPanelView mColor5;

	private OnColorChangedListener mListener;

	public interface OnColorChangedListener {
		public void onColorChanged(int color);
	}
	
	public ColorPickerDialog(Context context, int initialColor) {
		super(context);

		init(initialColor);
	}

	private void init(int color) {
		// To fight color banding.
		getWindow().setFormat(PixelFormat.RGBA_8888);

		setUp(color);

	}

	private void setUp(int color) {
		
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View layout = inflater.inflate(R.layout.dialog_color_picker, null);

		setContentView(layout);

		setTitle(R.string.dialog_color_picker);
		
		mColorPicker = (ColorPickerView) layout.findViewById(R.id.color_picker_view);
		mOldColor = (ColorPickerPanelView) layout.findViewById(R.id.old_color_panel);
		mNewColor = (ColorPickerPanelView) layout.findViewById(R.id.new_color_panel);
		mColor1 = (ColorPickerPanelView) layout.findViewById(R.id.color_panel_1);
		mColor2 = (ColorPickerPanelView) layout.findViewById(R.id.color_panel_2);
		mColor3 = (ColorPickerPanelView) layout.findViewById(R.id.color_panel_3);
		mColor4 = (ColorPickerPanelView) layout.findViewById(R.id.color_panel_4);
		mColor5 = (ColorPickerPanelView) layout.findViewById(R.id.color_panel_5);
		
		((LinearLayout) mOldColor.getParent()).setPadding(
			Math.round(mColorPicker.getDrawingOffset()), 
			0, 
			Math.round(mColorPicker.getDrawingOffset()), 
			0
		);	
		
		mOldColor.setOnClickListener(this);
		mNewColor.setOnClickListener(this);
		mColor1.setOnClickListener(this);
		mColor2.setOnClickListener(this);
		mColor3.setOnClickListener(this);
		mColor4.setOnClickListener(this);
		mColor5.setOnClickListener(this);
		mColorPicker.setOnColorChangedListener(this);
		mOldColor.setColor(color);
		mColorPicker.setColor(color, true);
		
		mColor1.setColor(Color.argb(255, 253, 201, 63 ));
		mColor2.setColor(Color.argb(255, 151, 253, 63 ));
		mColor3.setColor(Color.argb(255,  63, 166, 253));
		mColor4.setColor(Color.argb(255, 253,  63, 63 ));
		mColor5.setColor(Color.argb(255, 143, 143, 143));
	}

	@Override
	public void onColorChanged(int color) {

		mNewColor.setColor(color);

		/*
		if (mListener != null) {
			mListener.onColorChanged(color);
		}
		*/

	}

	public void setAlphaSliderVisible(boolean visible) {
		mColorPicker.setAlphaSliderVisible(visible);
	}
	
	/**
	 * Set a OnColorChangedListener to get notified when the color
	 * selected by the user has changed.
	 * @param listener
	 */
	public void setOnColorChangedListener(OnColorChangedListener listener){
		mListener = listener;
	}

	public int getColor() {
		return mColorPicker.getColor();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.new_color_panel) {
			if (mListener != null) {
				mListener.onColorChanged(mNewColor.getColor());
			}
		}
		
		if (v.getId() == R.id.color_panel_1) if (mListener != null) 
				mListener.onColorChanged(mColor1.getColor());
		if (v.getId() == R.id.color_panel_2) if (mListener != null) 
			mListener.onColorChanged(mColor2.getColor());
		if (v.getId() == R.id.color_panel_3) if (mListener != null) 
			mListener.onColorChanged(mColor3.getColor());
		if (v.getId() == R.id.color_panel_4) if (mListener != null) 
			mListener.onColorChanged(mColor4.getColor());
		if (v.getId() == R.id.color_panel_5) if (mListener != null) 
			mListener.onColorChanged(mColor5.getColor());
		
		dismiss();
	}
	
	@Override
	public Bundle onSaveInstanceState() {
		Bundle state = super.onSaveInstanceState();
		state.putInt("old_color", mOldColor.getColor());
		state.putInt("new_color", mNewColor.getColor());
		return state;
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mOldColor.setColor(savedInstanceState.getInt("old_color"));
		mColorPicker.setColor(savedInstanceState.getInt("new_color"), true);
	}
}
