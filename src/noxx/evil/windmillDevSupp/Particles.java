package noxx.evil.windmillDevSupp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import noxx.evil.windmillDevSupp.LiveWallpaper.myMetrics;

public class Particles {
	
	public Sprite[] particles;
	private float tempX, tempY, tempRotSpeed;
	private Random gen;
	private int N_PARTICLES;
	private boolean textureLoaded = false;
	public int[] mTextureName;	
	public int[] mCrop;
	public int imageHeight, imageWidth;
	public int visibleParticles = 4;
	public float speedMultiplier = 1.0f;
	public float scaleSetting = 1.0f;
	private Square square = new Square();

	public Particles(Resources resources, int _N_PARTICLES, myMetrics display, float scale, float xOffset, float yOffset) {
    	gen = new Random();
		N_PARTICLES = _N_PARTICLES;
    	particles = new Sprite[N_PARTICLES];
    	for (int i = 0; i < N_PARTICLES; i++) {
    		tempX = (float)display.width / (visibleParticles+1) * (i + 1) - xOffset; 
    		tempY = (float)display.height / 2 - yOffset;
    		tempRotSpeed = gen.nextFloat() * 25 + 10;
    		particles[i] = new Sprite(tempX, tempY, scale, scale, tempRotSpeed);
    	}
	}
	
	private class Square {
		private FloatBuffer vertexBuffer;	// buffer holding the vertices
		
		private float vertices[] = {
				-1.0f, -1.0f,  0.0f,		// V1 - bottom left
				-1.0f,  1.0f,  0.0f,		// V2 - top left
				 1.0f, -1.0f,  0.0f,		// V3 - bottom right
				 1.0f,  1.0f,  0.0f			// V4 - top right
		};
		
		private FloatBuffer textureBuffer;	// buffer holding the texture coordinates
		private float texture[] = {    		
				// Mapping coordinates for the vertices
				0.0f, 1.0f,		// top left		(V2)
				0.0f, 0.0f,		// bottom left	(V1)
				1.0f, 1.0f,		// top right	(V4)
				1.0f, 0.0f		// bottom right	(V3)
		};
		
		public Square() {
			// a float has 4 bytes so we allocate for each coordinate 4 bytes
			ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
			vertexByteBuffer.order(ByteOrder.nativeOrder());
			
			// allocates the memory from the byte buffer
			vertexBuffer = vertexByteBuffer.asFloatBuffer();
			
			// fill the vertexBuffer with the vertices
			vertexBuffer.put(vertices);
			
			// set the cursor position to the beginning of the buffer
			vertexBuffer.position(0);
			
			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
			byteBuffer.order(ByteOrder.nativeOrder());
			textureBuffer = byteBuffer.asFloatBuffer();
			textureBuffer.put(texture);
			textureBuffer.position(0);
		}

		/** The draw method for the square with the GL context */
		public void draw(GL10 gl) {
			// bind the previously generated texture
			gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
			
			// Point to our buffers
			gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
			
			// Set the face rotation
			gl.glFrontFace(GL10.GL_CW);
			
			// Point to our vertex buffer
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
			gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
			
			// Draw the vertices as triangle strip
			gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

			//Disable the client state before leaving
			gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void loadTexture(GL10 gl, Resources resources, int resourceId) {
		if (!textureLoaded) {
    		mTextureName = new int[1];
	    	// Generate Texture ID
	    	gl.glGenTextures(1, mTextureName, 0);
	    	// Did generate work? 
	    	assert gl.glGetError() == GL10.GL_NO_ERROR;
	    	
	    	InputStream is = resources.openRawResource(resourceId);
	    	Bitmap bitmap;
		    try {
		    	bitmap = BitmapFactory.decodeStream(is);
		    } finally {
		    	try {
		    		is.close();
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    }
		
		    // Bind texture id texturing target (we want 2D of course)
		    gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
		    	
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
		    	     	 
		    // Build our crop texture to be the size of the bitmap (ie full texture)
		    mCrop = new int[4];
		    mCrop[0] = 0;
		    mCrop[1] = imageHeight = bitmap.getHeight();
		    mCrop[2] = imageWidth = bitmap.getWidth();
		    mCrop[3] = -bitmap.getHeight();
		    	
		    ((GL11)gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mCrop, 0);
		    	 
		    // Magic Android function that setups up the internal formatting
		    // of the bitmap for OpenGL ES
		    GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		    	 
		    // Did texImage work?
		    assert gl.glGetError() == GL10.GL_NO_ERROR;
				
		    bitmap.recycle();
	    	
	    	for (int i = 0; i < N_PARTICLES; i++) {
	    		particles[i].imageHeight = imageHeight;
	    		particles[i].imageWidth = imageWidth;
	    	}
	    	
	    	textureLoaded = true;
    	}
	}
//	
//	public void reloadTexture(GL10 gl, Resources resources, int resourceId) {
//		gl.glDeleteTextures(1, mTextureName, 0);
//		textureLoaded = false;
//		loadTexture(gl, resources, resourceId);
//	}
	
	public void draw(GL10 gl) {
		square.draw(gl);
	}
	
	public void drawSprites(GL10 gl) {
//		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
//		for (int i = 0; i < visibleParticles; i++) {
//	    	((GL11Ext)gl).glDrawTexfOES(particles[i].x, particles[i].y, 0, imageWidth * particles[i].scaleX, imageHeight * particles[i].scaleY);
//		}
		for (int i = 0; i < visibleParticles; i++) {
			gl.glLoadIdentity();
			gl.glTranslatef(particles[i].x, particles[i].y, 1.0f);
			gl.glScalef(particles[i].scaleX*scaleSetting, particles[i].scaleY*scaleSetting, 1.0f);
			gl.glRotatef(particles[i].rot, 0, 0, 1);
			square.draw(gl);
		}
	}
	
	public void updateSprites(float delta, myMetrics display) {
		for (int i = 0; i < visibleParticles; i++) {
			particles[i].update(delta, display, speedMultiplier);
		}
	}
}