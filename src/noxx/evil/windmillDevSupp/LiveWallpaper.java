package noxx.evil.windmillDevSupp;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import net.rbgrn.android.glwallpaperservice.GLWallpaperService;
import noxx.evil.windmillDevSupp.R;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.opengl.GLU;
import android.util.DisplayMetrics;

public class LiveWallpaper extends GLWallpaperService
{

	static final String SHARED_PREFS_NAME = "Windmill Preferences";
	private SharedPreferences mPreferences;
	
	public LiveWallpaper() {
		super();
	}
	
	public Engine onCreateEngine() {
		MyEngine engine = new MyEngine(this.getResources());
		return engine;
	}
	
	class MyEngine extends GLEngine implements SharedPreferences.OnSharedPreferenceChangeListener {
		
		ClearRenderer renderer;
		private int width, height;
		private float scaleFactor;											
		public MyEngine(Resources resources) {
			super();
			DisplayMetrics metrics = new DisplayMetrics();
			metrics = resources.getDisplayMetrics();
			scaleFactor = metrics.density / 2.0f;
			width = metrics.widthPixels;
			height = metrics.heightPixels;
			
			this.setEGLConfigChooser(8,8,8,8,0,0);
			renderer = new ClearRenderer(resources, width, height, scaleFactor);
			setRenderer(renderer);
			setRenderMode(RENDERMODE_CONTINUOUSLY);
			
			mPreferences = LiveWallpaper.this.getSharedPreferences(SHARED_PREFS_NAME, 0);
			mPreferences.registerOnSharedPreferenceChangeListener(this);
			onSharedPreferenceChanged(mPreferences, null);
			onSharedPreferenceChanged(mPreferences, "color_scheme_key");
		}

		public void onDestroy() {
			super.onDestroy();
			if (renderer != null) {
				//renderer.release(); // assuming yours has this method - it should!
			}
			renderer = null;
		}

		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {			
			try {
				renderer.background.setColor(sharedPreferences.getInt("bgColor", 0xfffdc93f));
				/*renderer.background.setColor((int)Long.parseLong(sharedPreferences.getString("prebgColor", "fffdc93f"),16));*/
				int newWindmillCount = sharedPreferences.getInt("windmillCount", 4);
				renderer.helie.visibleParticles = newWindmillCount;
				renderer.poles.visibleParticles = newWindmillCount;
				renderer.globalYoffset = -sharedPreferences.getInt("globalYoffset", 0);
				renderer.helie.scaleSetting = (float)sharedPreferences.getInt("helixScale", 100)/100;
				renderer.drawTopFade = sharedPreferences.getBoolean("topfade_checkbox", true);
				renderer.windmillYoffset = -sharedPreferences.getInt("windmillYoffset", 0);
				
//				int newFPS = sharedPreferences.getInt("fps", 30);
//				renderer.thousandOverFPS = 1000 / newFPS;
//				int newParticleCount = sharedPreferences.getInt("particleCount", 15);
//				renderer.particles.visibleParticles = newParticleCount;
//				int newSpeedMult = sharedPreferences.getInt("speedMultiplier", 100);
//				renderer.particles.speedMultiplier = (float)newSpeedMult / 100.0f;
//				if (key.equals("color_scheme_key")) {
//					int newTextureIndex = Integer.parseInt(sharedPreferences.getString("color_scheme_key", "0"));
//					renderer.newBallResourceId = ballResources[newTextureIndex];
//					renderer.newBackgroundResourceId = backgroundResources[newTextureIndex];
//					renderer.needsReloading = true;
//				}
			} catch (NullPointerException e) {
				// Move along, nothing to see here...
			}
			
		}
	}
	
	class ClearRenderer implements GLWallpaperService.Renderer {
		private Resources _resources;
		private Particles helie;
		private TexturelessSprite background;
		private Sprite horizon;
		private Sprite topfade;
		private Particles poles;
		private long lastTime;
		private long currentTime;
		private float delta;
		private myMetrics display;
		private final int N_PARTICLES = 5;
		private float scaleFactor = 1.0f;
		
		public int globalYoffset = 0;
		public int windmillYoffset = 0;
		
		private boolean drawTopFade = true;
		
		public int thousandOverFPS = 33;
		private long dt;
//		private int fps = 0;
//		private float tempDt = 0;
		
	    public ClearRenderer(Resources resources, int width, int height, float _scaleFactor) {
	    	_resources = resources;
	    	scaleFactor = _scaleFactor;
	    	display = new myMetrics(width, height);
	    	helie = new Particles(_resources, N_PARTICLES, display, 60*scaleFactor, 0, -50*scaleFactor-globalYoffset-windmillYoffset);
	    	poles = new Particles(_resources, N_PARTICLES, display, 1*scaleFactor, 7*scaleFactor, 200*scaleFactor-globalYoffset-windmillYoffset);
	    	topfade = new Sprite(0, display.height - display.height/6, 1, 1, 0);
	    	horizon = new Sprite(0, height / 2 - 256*scaleFactor - 128-globalYoffset, 1, 1.0f, 0);
	    	background = new TexturelessSprite(0, height / 2 - 256*scaleFactor - 128-globalYoffset, display.width, display.height);
	   	}

	    public void onDrawFrame(GL10 gl) {
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

			currentTime = System.currentTimeMillis();
	    	delta = (float)(currentTime - lastTime) / 1000.0f;
			
	    	dt = currentTime - lastTime;
	    	if (dt < thousandOverFPS) {
				try {
					Thread.sleep(thousandOverFPS - dt);
//					tempDt += thousandOverFPS - dt;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
	    	}
	    	lastTime = currentTime;
	    	
			gl.glLoadIdentity();
			GLU.gluLookAt(gl, 
					0.0f, 0.0f, 0.0f, 
					0.0f, 0.0f, -1.0f, 
					0.0f, 1.0f, 0.0f);

			background.draw(gl);
			for ( int i=0; i<poles.visibleParticles; i++) {
				poles.particles[i].draw(gl);
			}
			helie.drawSprites(gl);
			horizon.draw(gl);
			
			if (drawTopFade) topfade.draw(gl);
			
			helie.updateSprites(delta, display);
			
		}

		public void onSurfaceChanged(GL10 gl, int width, int height) {
			if(height == 0) { 						//Prevent A Divide By Zero By
				height = 1; 						//Making Height Equal One
			}
			display.width = width;
	    	display.height = height;
			gl.glViewport(0, 0, width, height); 	//Reset The Current Viewport
			gl.glMatrixMode(GL10.GL_PROJECTION); 	//Select The Projection Matrix
			gl.glLoadIdentity(); 					//Reset The Projection Matrix

			float ratio = (float) width / height;
			gl.glFrustumf(-ratio, ratio, -1, 1, -100.0f, 100.0f);
			gl.glOrthof(0.0f, display.width, 0.0f, display.height, -100.0f, 100.0f);

			gl.glMatrixMode(GL10.GL_MODELVIEW); 	//Select The Modelview Matrix
			gl.glLoadIdentity(); 					//Reset The Modelview Matrix
			
			float heliexOffset, helieyOffset;
			float polexOffset, poleyOffset;
			if (height < width) { // landscape mode
				heliexOffset = 0;
				helieyOffset = -50*scaleFactor;
				polexOffset = 7*scaleFactor;
				poleyOffset = 200*scaleFactor;
				//horizon.scaleY = 1.0f;
			} else {
				heliexOffset = 0;
				helieyOffset = -50*scaleFactor;
				polexOffset = 7*scaleFactor;
				poleyOffset = 200*scaleFactor;
				//horizon.scaleY = 1.0f;
			}
			
			for (int i = 0; i < helie.visibleParticles; i++) {
				helie.particles[i].x = (float)display.width / (helie.visibleParticles+1) * (i + 1) - heliexOffset;
				helie.particles[i].y = (float)display.height / 2 - helieyOffset - globalYoffset - windmillYoffset;
				poles.particles[i].x = (float)display.width / (poles.visibleParticles+1) * (i + 1) - polexOffset;
				poles.particles[i].y = (float)display.height / 2 - poleyOffset - globalYoffset - windmillYoffset; 
			}
			
			float backgroundY = height / 2 - 256*scaleFactor - 128 - globalYoffset;
			
			background.x = 0;
			background.y = backgroundY;
			background.scaleX = display.width;
			background.scaleY = display.height;
			
			horizon.x = 0;
			horizon.y = backgroundY;
			horizon.scaleX = (float)width / horizon.imageWidth;
			
			topfade.x = 0;
			topfade.y = height - height/6;
			topfade.scaleX = width / topfade.imageWidth;
			topfade.scaleY = (float)(height/3) / topfade.imageHeight;
		}
		
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			helie.loadTexture(gl, _resources, R.drawable.helix);
			for ( int i=0; i<N_PARTICLES; i++) {
				poles.particles[i].loadTexture(gl, _resources, R.drawable.pole);
			}
			topfade.loadTexture(gl, _resources, R.drawable.topfade);
			topfade.scaleX = display.width / topfade.imageWidth;
			topfade.scaleY = (float)(display.height/3) / topfade.imageHeight;
			
			horizon.loadTexture(gl, _resources, R.drawable.horizon);
			horizon.scaleX = (float)display.width / horizon.imageWidth;
			
			gl.glEnable(GL10.GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
//			gl.glClearColor(0.99f, 0.79f, 0.25f, 1.0f); 	//Black Background
			gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); 	//Black Background
			// gl.glClearDepthf(1.0f); 					//Depth Buffer Setup
//			gl.glEnable(GL10.GL_DEPTH_TEST); 			//Enables Depth Testing
//			gl.glDepthFunc(GL10.GL_LEQUAL); 			//The Type Of Depth Testing To Do
//			gl.glEnable(GL10.GL_ALPHA_TEST);
//			gl.glAlphaFunc(GL10.GL_GEQUAL, 0.5f);
			
			gl.glShadeModel(GL10.GL_FLAT);
	    	gl.glDisable(GL10.GL_DITHER);
	    	gl.glDepthMask(false);
	    	
	    	// gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_DST_ALPHA);
//	    	gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
	    	gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
	    	gl.glEnable(GL10.GL_BLEND);
	    	
	    	gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);
	    	
			//gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST); 
		}
	}
	public class myMetrics {
		public int width, height;
		public myMetrics(int w, int h) {
			width = w;
			height = h;
		}
	}
}