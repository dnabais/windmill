package noxx.evil.windmillDevSupp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Color;

public class TexturelessSprite {
	
		private FloatBuffer vertexBuffer;	// buffer holding the vertices
		public float x, y;
		public float scaleX, scaleY;
		public float red, blue, green;
		
		private float vertices[] = {
				0.0f, 0.0f,  0.0f,		// V1 - bottom left
				0.0f, 1.0f,  0.0f,		// V2 - top left
				1.0f, 0.0f,  0.0f,		// V3 - bottom right
				1.0f, 1.0f,  0.0f		// V4 - top right
		};
				
		public TexturelessSprite(float _x, float _y, float _scaleX, float _scaleY) {
			x = _x;
			y = _y;
			scaleX = _scaleX;
			scaleY = _scaleY;
			red = 1.0f;
			green = 0.9f;
			blue = 0.05f;
			ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
			vertexByteBuffer.order(ByteOrder.nativeOrder());
			vertexBuffer = vertexByteBuffer.asFloatBuffer();
			vertexBuffer.put(vertices);
			vertexBuffer.position(0);
		}

		public void draw(GL10 gl) {
			gl.glDisable(GL10.GL_TEXTURE_2D);
			gl.glLoadIdentity();
			gl.glColor4f(red, green, blue, 1.0f);
			gl.glTranslatef(x, y, 0.0f);
			gl.glScalef(scaleX, scaleY, 1);
			gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glFrontFace(GL10.GL_CW);			
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
			gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
			gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glEnable(GL10.GL_TEXTURE_2D);
		}
		
		public void setColor(int newColorInt) {
			red   = (float)Color.red(newColorInt) / 255.0f;
			green = (float)Color.green(newColorInt) / 255.0f;
			blue  = (float)Color.blue(newColorInt) / 255.0f;
		}
	}