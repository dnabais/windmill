package noxx.evil.windmillDevSupp;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import noxx.evil.windmillDevSupp.LiveWallpaper.myMetrics;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class Sprite {
	
	public float x = 0, y = 0, dx = 0, dy = 0;
	public float scaleX = 1, scaleY = 1;
	public float rot = 0;
	public float rotationSpeed = 1;
	
	public int[] mTextureName;
	public int[] mCrop;
	public int imageHeight, imageWidth;
		
	private boolean textureLoaded = false;
	
	public Sprite(float _x, float _y, float _scaleX, float _scaleY, float _rotationSpeed) {
		x = _x;
		y = _y;
		scaleX = _scaleX;
		scaleY = _scaleY;
		rotationSpeed = _rotationSpeed;
	}
	
	public void loadTexture(GL10 gl, Resources resources, int resourceId) {
		if (!textureLoaded) {
    		mTextureName = new int[1];
	    	// Generate Texture ID
	    	gl.glGenTextures(1, mTextureName, 0);
	    	// Did generate work? 
	    	assert gl.glGetError() == GL10.GL_NO_ERROR;
	    	
	    	InputStream is = resources.openRawResource(resourceId);
	    	Bitmap bitmap;
		    try {
		    	bitmap = BitmapFactory.decodeStream(is);
		    } finally {
		    	try {
		    		is.close();
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    }
		
		    // Bind texture id texturing target (we want 2D of course)
		    gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]);
		    	
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
		    	     	 
		    // Build our crop texture to be the size of the bitmap (ie full texture)
		    mCrop = new int[4];
		    mCrop[0] = 0;
		    mCrop[1] = imageHeight = bitmap.getHeight();
		    mCrop[2] = imageWidth = bitmap.getWidth();
		    mCrop[3] = -bitmap.getHeight();
		    	
		    ((GL11)gl).glTexParameteriv(GL10.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mCrop, 0);
		    	 
		    // Magic Android function that setups up the internal formatting
		    // of the bitmap for OpenGL ES
		    GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		    	 
		    // Did texImage work?
		    assert gl.glGetError() == GL10.GL_NO_ERROR;
		    bitmap.recycle();
	    	
	    	textureLoaded = true;
    	}
	}

	public void reloadTexture(GL10 gl, Resources resources, int resourceId) {
		gl.glDeleteTextures(1, mTextureName, 0);
		textureLoaded = false;
		loadTexture(gl, resources, resourceId);
	}

	public void draw(GL10 gl) {
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureName[0]); 
    	((GL11Ext)gl).glDrawTexfOES(x, y, 0, imageWidth * scaleX, imageHeight * scaleY);
	}
	
	public void update(float delta, myMetrics display, float speedMult) {
		rot += delta * speedMult * rotationSpeed;
		rot %= 360;
	}
}